package com.zzm.demo.service.impl;

import com.zzm.demo.dao.IUserDao;
import com.zzm.demo.pojo.User;
import com.zzm.demo.service.IUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements IUserService {
    static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
    @Autowired
    IUserDao userDao;

    @Override
    public void saveUser(User user) {
        User user_copy = userDao.save(user);
        logger.info(user_copy.toString());
    }

    @Override
    public List<User> findUsersByName(String name) {
        return userDao.findByName(name);
    }
}
