package com.zzm.demo.service;

import com.zzm.demo.pojo.User;

import java.util.List;

public interface IUserService {
    void saveUser(User user);

    List<User> findUsersByName(String name);
}
